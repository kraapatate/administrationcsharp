﻿namespace P2022_FSI_Administration.WinForm
{
    partial class AjouterCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AjouterCours));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.tbAEDescCours = new System.Windows.Forms.TextBox();
            this.tbAENomCours = new System.Windows.Forms.TextBox();
            this.lPrenom = new System.Windows.Forms.Label();
            this.lNom = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbClasse = new System.Windows.Forms.ComboBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022AppliAdministrationDataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1();
            this.classeTableAdapter1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022AppliAdministrationDataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(77, 87);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(220, 278);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(195, 77);
            this.bEnregistrer.TabIndex = 26;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(12, 278);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(190, 77);
            this.bEffacer.TabIndex = 25;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            // 
            // tbAEDescCours
            // 
            this.tbAEDescCours.Location = new System.Drawing.Point(178, 161);
            this.tbAEDescCours.Name = "tbAEDescCours";
            this.tbAEDescCours.Size = new System.Drawing.Size(221, 20);
            this.tbAEDescCours.TabIndex = 24;
            // 
            // tbAENomCours
            // 
            this.tbAENomCours.Location = new System.Drawing.Point(178, 99);
            this.tbAENomCours.Name = "tbAENomCours";
            this.tbAENomCours.Size = new System.Drawing.Size(221, 20);
            this.tbAENomCours.TabIndex = 23;
            // 
            // lPrenom
            // 
            this.lPrenom.AutoSize = true;
            this.lPrenom.Location = new System.Drawing.Point(81, 164);
            this.lPrenom.Name = "lPrenom";
            this.lPrenom.Size = new System.Drawing.Size(90, 13);
            this.lPrenom.TabIndex = 21;
            this.lPrenom.Text = "Description Cours";
            // 
            // lNom
            // 
            this.lNom.AutoSize = true;
            this.lNom.Location = new System.Drawing.Point(112, 102);
            this.lNom.Name = "lNom";
            this.lNom.Size = new System.Drawing.Size(59, 13);
            this.lNom.TabIndex = 20;
            this.lNom.Text = "Nom Cours";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(134, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 28);
            this.label3.TabIndex = 29;
            this.label3.Text = "Ajouter un cours";
            // 
            // cbClasse
            // 
            this.cbClasse.DataSource = this.bindingSource1;
            this.cbClasse.DisplayMember = "libelleclasse";
            this.cbClasse.FormattingEnabled = true;
            this.cbClasse.Location = new System.Drawing.Point(178, 214);
            this.cbClasse.Name = "cbClasse";
            this.cbClasse.Size = new System.Drawing.Size(221, 21);
            this.cbClasse.TabIndex = 30;
            this.cbClasse.ValueMember = "idclasse";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "classe";
            this.bindingSource1.DataSource = this.p2022AppliAdministrationDataSet1BindingSource;
            // 
            // p2022AppliAdministrationDataSet1BindingSource
            // 
            this.p2022AppliAdministrationDataSet1BindingSource.DataSource = this.p2022_Appli_AdministrationDataSet1;
            this.p2022AppliAdministrationDataSet1BindingSource.Position = 0;
            // 
            // p2022_Appli_AdministrationDataSet1
            // 
            this.p2022_Appli_AdministrationDataSet1.DataSetName = "P2022_Appli_AdministrationDataSet1";
            this.p2022_Appli_AdministrationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // classeTableAdapter1
            // 
            this.classeTableAdapter1.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Classe";
            // 
            // AjouterCours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbClasse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Controls.Add(this.tbAEDescCours);
            this.Controls.Add(this.tbAENomCours);
            this.Controls.Add(this.lPrenom);
            this.Controls.Add(this.lNom);
            this.Name = "AjouterCours";
            this.Text = "Ajoutercours";
            this.Load += new System.EventHandler(this.AjouterCours_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022AppliAdministrationDataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.TextBox tbAEDescCours;
        private System.Windows.Forms.TextBox tbAENomCours;
        private System.Windows.Forms.Label lPrenom;
        private System.Windows.Forms.Label lNom;
        private System.Windows.Forms.Label label3;

        private P2022_Appli_AdministrationDataSet2 p2022_Appli_AdministrationDataSet2;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private P2022_Appli_AdministrationDataSet2TableAdapters.classeTableAdapter classeTableAdapter;
        private System.Windows.Forms.ComboBox cbClasse;
        private System.Windows.Forms.BindingSource p2022AppliAdministrationDataSet1BindingSource;
        private P2022_Appli_AdministrationDataSet1 p2022_Appli_AdministrationDataSet1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter classeTableAdapter1;
        private System.Windows.Forms.Label label1;
    }
}