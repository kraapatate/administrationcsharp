﻿namespace P2022_FSI_Administration.WinForm
{
    partial class ListeCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListeCours));
            this.bQuitter = new System.Windows.Forms.Button();
            this.msGlobal = new System.Windows.Forms.MenuStrip();
            this.accueilToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionEtudiantToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesEtudiantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnEtudiantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesClassesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUneClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvListeCours = new System.Windows.Forms.DataGridView();
            this.idcoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.libellecoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptioncoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idclasseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coursBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet2 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet2();
            this.coursTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet2TableAdapters.coursTableAdapter();
            this.msGlobal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeCours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coursBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet2)).BeginInit();
            this.SuspendLayout();
            // 
            // bQuitter
            // 
            this.bQuitter.BackColor = System.Drawing.Color.LightCyan;
            this.bQuitter.Location = new System.Drawing.Point(597, 173);
            this.bQuitter.Name = "bQuitter";
            this.bQuitter.Size = new System.Drawing.Size(171, 61);
            this.bQuitter.TabIndex = 19;
            this.bQuitter.Text = "QUITTER";
            this.bQuitter.UseVisualStyleBackColor = false;
            this.bQuitter.Click += new System.EventHandler(this.bQuitter_Click_1);
            // 
            // msGlobal
            // 
            this.msGlobal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accueilToolStripMenuItem2,
            this.gestionEtudiantToolStripMenuItem1,
            this.gestionClasseToolStripMenuItem,
            this.gestionCoursToolStripMenuItem});
            this.msGlobal.Location = new System.Drawing.Point(0, 0);
            this.msGlobal.Name = "msGlobal";
            this.msGlobal.Size = new System.Drawing.Size(800, 24);
            this.msGlobal.TabIndex = 18;
            this.msGlobal.Text = "Menu";
            // 
            // accueilToolStripMenuItem2
            // 
            this.accueilToolStripMenuItem2.Name = "accueilToolStripMenuItem2";
            this.accueilToolStripMenuItem2.Size = new System.Drawing.Size(58, 20);
            this.accueilToolStripMenuItem2.Text = "Accueil";
            this.accueilToolStripMenuItem2.Click += new System.EventHandler(this.accueilToolStripMenuItem2_Click);
            // 
            // gestionEtudiantToolStripMenuItem1
            // 
            this.gestionEtudiantToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesEtudiantsToolStripMenuItem,
            this.ajouterUnEtudiantToolStripMenuItem});
            this.gestionEtudiantToolStripMenuItem1.Name = "gestionEtudiantToolStripMenuItem1";
            this.gestionEtudiantToolStripMenuItem1.Size = new System.Drawing.Size(106, 20);
            this.gestionEtudiantToolStripMenuItem1.Text = "Gestion Etudiant";
            // 
            // listeDesEtudiantsToolStripMenuItem
            // 
            this.listeDesEtudiantsToolStripMenuItem.Name = "listeDesEtudiantsToolStripMenuItem";
            this.listeDesEtudiantsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.listeDesEtudiantsToolStripMenuItem.Text = "Liste des étudiants";
            this.listeDesEtudiantsToolStripMenuItem.Click += new System.EventHandler(this.listeDesEtudiantsToolStripMenuItem_Click);
            // 
            // ajouterUnEtudiantToolStripMenuItem
            // 
            this.ajouterUnEtudiantToolStripMenuItem.Name = "ajouterUnEtudiantToolStripMenuItem";
            this.ajouterUnEtudiantToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.ajouterUnEtudiantToolStripMenuItem.Text = "Ajouter un étudiant";
            this.ajouterUnEtudiantToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnEtudiantToolStripMenuItem_Click);
            // 
            // gestionClasseToolStripMenuItem
            // 
            this.gestionClasseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesClassesToolStripMenuItem,
            this.ajouterUneClasseToolStripMenuItem});
            this.gestionClasseToolStripMenuItem.Name = "gestionClasseToolStripMenuItem";
            this.gestionClasseToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.gestionClasseToolStripMenuItem.Text = "Gestion classe";
            // 
            // listeDesClassesToolStripMenuItem
            // 
            this.listeDesClassesToolStripMenuItem.Name = "listeDesClassesToolStripMenuItem";
            this.listeDesClassesToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.listeDesClassesToolStripMenuItem.Text = "Liste des classes";
            this.listeDesClassesToolStripMenuItem.Click += new System.EventHandler(this.listeDesClassesToolStripMenuItem_Click);
            // 
            // ajouterUneClasseToolStripMenuItem
            // 
            this.ajouterUneClasseToolStripMenuItem.Name = "ajouterUneClasseToolStripMenuItem";
            this.ajouterUneClasseToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.ajouterUneClasseToolStripMenuItem.Text = "Ajouter une classe";
            this.ajouterUneClasseToolStripMenuItem.Click += new System.EventHandler(this.ajouterUneClasseToolStripMenuItem_Click);
            // 
            // gestionCoursToolStripMenuItem
            // 
            this.gestionCoursToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesCoursToolStripMenuItem,
            this.ajouterCoursToolStripMenuItem});
            this.gestionCoursToolStripMenuItem.Name = "gestionCoursToolStripMenuItem";
            this.gestionCoursToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.gestionCoursToolStripMenuItem.Text = "Gestion cours";
            // 
            // listeDesCoursToolStripMenuItem
            // 
            this.listeDesCoursToolStripMenuItem.Name = "listeDesCoursToolStripMenuItem";
            this.listeDesCoursToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.listeDesCoursToolStripMenuItem.Text = "Liste des cours";
            this.listeDesCoursToolStripMenuItem.Click += new System.EventHandler(this.listeDesCoursToolStripMenuItem_Click);
            // 
            // ajouterCoursToolStripMenuItem
            // 
            this.ajouterCoursToolStripMenuItem.Name = "ajouterCoursToolStripMenuItem";
            this.ajouterCoursToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.ajouterCoursToolStripMenuItem.Text = "Ajouter des cours";
            this.ajouterCoursToolStripMenuItem.Click += new System.EventHandler(this.ajouterCoursToolStripMenuItem_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.Location = new System.Drawing.Point(10, 50);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(78, 87);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 17;
            this.pbLogo.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(227, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 28);
            this.label3.TabIndex = 30;
            this.label3.Text = "Liste des cours";
            // 
            // dgvListeCours
            // 
            this.dgvListeCours.AutoGenerateColumns = false;
            this.dgvListeCours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListeCours.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idcoursDataGridViewTextBoxColumn,
            this.libellecoursDataGridViewTextBoxColumn,
            this.descriptioncoursDataGridViewTextBoxColumn,
            this.idclasseDataGridViewTextBoxColumn});
            this.dgvListeCours.DataSource = this.coursBindingSource;
            this.dgvListeCours.Location = new System.Drawing.Point(112, 108);
            this.dgvListeCours.Name = "dgvListeCours";
            this.dgvListeCours.Size = new System.Drawing.Size(448, 150);
            this.dgvListeCours.TabIndex = 31;
            this.dgvListeCours.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListeCours_CellContentClick_1);
            // 
            // idcoursDataGridViewTextBoxColumn
            // 
            this.idcoursDataGridViewTextBoxColumn.DataPropertyName = "idcours";
            this.idcoursDataGridViewTextBoxColumn.HeaderText = "idcours";
            this.idcoursDataGridViewTextBoxColumn.Name = "idcoursDataGridViewTextBoxColumn";
            this.idcoursDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcoursDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // libellecoursDataGridViewTextBoxColumn
            // 
            this.libellecoursDataGridViewTextBoxColumn.DataPropertyName = "libellecours";
            this.libellecoursDataGridViewTextBoxColumn.HeaderText = "libellecours";
            this.libellecoursDataGridViewTextBoxColumn.Name = "libellecoursDataGridViewTextBoxColumn";
            this.libellecoursDataGridViewTextBoxColumn.ReadOnly = true;
            this.libellecoursDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // descriptioncoursDataGridViewTextBoxColumn
            // 
            this.descriptioncoursDataGridViewTextBoxColumn.DataPropertyName = "descriptioncours";
            this.descriptioncoursDataGridViewTextBoxColumn.HeaderText = "descriptioncours";
            this.descriptioncoursDataGridViewTextBoxColumn.Name = "descriptioncoursDataGridViewTextBoxColumn";
            this.descriptioncoursDataGridViewTextBoxColumn.ReadOnly = true;
            this.descriptioncoursDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // idclasseDataGridViewTextBoxColumn
            // 
            this.idclasseDataGridViewTextBoxColumn.DataPropertyName = "idclasse";
            this.idclasseDataGridViewTextBoxColumn.HeaderText = "idclasse";
            this.idclasseDataGridViewTextBoxColumn.Name = "idclasseDataGridViewTextBoxColumn";
            this.idclasseDataGridViewTextBoxColumn.ReadOnly = true;
            this.idclasseDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // coursBindingSource
            // 
            this.coursBindingSource.DataMember = "cours";
            this.coursBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet2;
            // 
            // p2022_Appli_AdministrationDataSet2
            // 
            this.p2022_Appli_AdministrationDataSet2.DataSetName = "P2022_Appli_AdministrationDataSet2";
            this.p2022_Appli_AdministrationDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // coursTableAdapter
            // 
            this.coursTableAdapter.ClearBeforeFill = true;
            // 
            // ListeCours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgvListeCours);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bQuitter);
            this.Controls.Add(this.msGlobal);
            this.Controls.Add(this.pbLogo);
            this.Name = "ListeCours";
            this.Text = "Listecours";
            this.Load += new System.EventHandler(this.ListeCours_Load);
            this.msGlobal.ResumeLayout(false);
            this.msGlobal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeCours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coursBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bQuitter;
        private System.Windows.Forms.MenuStrip msGlobal;
        private System.Windows.Forms.ToolStripMenuItem accueilToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gestionEtudiantToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listeDesEtudiantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnEtudiantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesClassesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUneClasseToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvListeCours;
        private P2022_Appli_AdministrationDataSet2 p2022_Appli_AdministrationDataSet2;
        private System.Windows.Forms.BindingSource coursBindingSource;
        private P2022_Appli_AdministrationDataSet2TableAdapters.coursTableAdapter coursTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem gestionCoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesCoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterCoursToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn idcoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn libellecoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptioncoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idclasseDataGridViewTextBoxColumn;
    }
}