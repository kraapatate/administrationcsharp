﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ModifierClasse : Form
        

    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;
        int id;
        public ModifierClasse(int id, string nomClasse)
        {
            InitializeComponent();
            this.id = id;
            tbNomClasse.Text = nomClasse;
        }
        private void reInitialisation()
        {
            tbNomClasse.Text = "";

        }

        private void bEnregistrer_Click(object sender, EventArgs e)
        {

            // ajout des variables
            string nomClasse = tbNomClasse.Text;
           
            

            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=OLEN@bts2022;";
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion.Open();



                // requete sql pour modifier les données de l'étudiant
                string comm = $"UPDATE classe SET libelleclasse = '{nomClasse}'" +
                    $"where idclasse = {id};";

                var cmd = new NpgsqlCommand(comm, maConnexion);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Cours modifié");
                reInitialisation();
            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }
            this.Close();
        }

        private void bEffacer_Click(object sender, EventArgs e)
        {

        }
        
    }
}
