﻿
namespace P2022_FSI_Administration
{
    partial class AjouterEtudiant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AjouterEtudiant));
            this.lNom = new System.Windows.Forms.Label();
            this.lPrenom = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbAENom = new System.Windows.Forms.TextBox();
            this.tbAEPrenom = new System.Windows.Forms.TextBox();
            this.bEffacer = new System.Windows.Forms.Button();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bRetour = new System.Windows.Forms.Button();
            this.cbClasse = new System.Windows.Forms.ComboBox();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1();
            this.p2022_Appli_AdministrationDataSet = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet();
            this.p2022AppliAdministrationDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022AppliAdministrationDataSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.classeTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAEMail = new System.Windows.Forms.TextBox();
            this.tbAETelephone = new System.Windows.Forms.TextBox();
            this.p2022_Appli_AdministrationDataSet4 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet4();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.etudiantTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet4TableAdapters.etudiantTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022AppliAdministrationDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022AppliAdministrationDataSetBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lNom
            // 
            this.lNom.AutoSize = true;
            this.lNom.Location = new System.Drawing.Point(54, 34);
            this.lNom.Name = "lNom";
            this.lNom.Size = new System.Drawing.Size(56, 25);
            this.lNom.TabIndex = 0;
            this.lNom.Text = "Nom";
            this.lNom.Click += new System.EventHandler(this.lNom_Click);
            // 
            // lPrenom
            // 
            this.lPrenom.AutoSize = true;
            this.lPrenom.Location = new System.Drawing.Point(30, 76);
            this.lPrenom.Name = "lPrenom";
            this.lPrenom.Size = new System.Drawing.Size(85, 25);
            this.lPrenom.TabIndex = 1;
            this.lPrenom.Text = "Prénom";
            this.lPrenom.Click += new System.EventHandler(this.lPrenom_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Classe";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbAENom
            // 
            this.tbAENom.Location = new System.Drawing.Point(123, 31);
            this.tbAENom.Name = "tbAENom";
            this.tbAENom.Size = new System.Drawing.Size(190, 38);
            this.tbAENom.TabIndex = 3;
            this.tbAENom.TextChanged += new System.EventHandler(this.tbAENom_TextChanged);
            // 
            // tbAEPrenom
            // 
            this.tbAEPrenom.Location = new System.Drawing.Point(123, 73);
            this.tbAEPrenom.Name = "tbAEPrenom";
            this.tbAEPrenom.Size = new System.Drawing.Size(190, 38);
            this.tbAEPrenom.TabIndex = 4;
            this.tbAEPrenom.TextChanged += new System.EventHandler(this.tbAEPrenom_TextChanged);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(2, 238);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(159, 40);
            this.bEffacer.TabIndex = 6;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            this.bEffacer.Click += new System.EventHandler(this.bEffacer_Click);
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(167, 238);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(164, 40);
            this.bEnregistrer.TabIndex = 7;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(46, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // bRetour
            // 
            this.bRetour.BackColor = System.Drawing.Color.LightCyan;
            this.bRetour.Location = new System.Drawing.Point(88, 284);
            this.bRetour.Name = "bRetour";
            this.bRetour.Size = new System.Drawing.Size(159, 40);
            this.bRetour.TabIndex = 9;
            this.bRetour.Text = "RETOUR";
            this.bRetour.UseVisualStyleBackColor = false;
            this.bRetour.Click += new System.EventHandler(this.bRetour_Click);
            // 
            // cbClasse
            // 
            this.cbClasse.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.classeBindingSource, "libelleclasse", true));
            this.cbClasse.DataSource = this.classeBindingSource;
            this.cbClasse.DisplayMember = "libelleclasse";
            this.cbClasse.FormattingEnabled = true;
            this.cbClasse.Location = new System.Drawing.Point(123, 114);
            this.cbClasse.Name = "cbClasse";
            this.cbClasse.Size = new System.Drawing.Size(190, 33);
            this.cbClasse.TabIndex = 10;
            this.cbClasse.ValueMember = "idclasse";
            this.cbClasse.SelectedIndexChanged += new System.EventHandler(this.cbClasse_SelectedIndexChanged);
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            this.classeBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet1;
            // 
            // p2022_Appli_AdministrationDataSet1
            // 
            this.p2022_Appli_AdministrationDataSet1.DataSetName = "P2022_Appli_AdministrationDataSet1";
            this.p2022_Appli_AdministrationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // p2022_Appli_AdministrationDataSet
            // 
            this.p2022_Appli_AdministrationDataSet.DataSetName = "P2022_Appli_AdministrationDataSet";
            this.p2022_Appli_AdministrationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // p2022AppliAdministrationDataSetBindingSource
            // 
            this.p2022AppliAdministrationDataSetBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet;
            this.p2022AppliAdministrationDataSetBindingSource.Position = 0;
            // 
            // p2022AppliAdministrationDataSetBindingSource1
            // 
            this.p2022AppliAdministrationDataSetBindingSource1.DataSource = this.p2022_Appli_AdministrationDataSet;
            this.p2022AppliAdministrationDataSetBindingSource1.Position = 0;
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(63, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(230, 28);
            this.label3.TabIndex = 30;
            this.label3.Text = "Ajouter un étudiant";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 25);
            this.label2.TabIndex = 31;
            this.label2.Text = "Mail";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 25);
            this.label4.TabIndex = 32;
            this.label4.Text = "Téléphone";
            // 
            // tbAEMail
            // 
            this.tbAEMail.Location = new System.Drawing.Point(123, 152);
            this.tbAEMail.Name = "tbAEMail";
            this.tbAEMail.Size = new System.Drawing.Size(190, 38);
            this.tbAEMail.TabIndex = 33;
            // 
            // tbAETelephone
            // 
            this.tbAETelephone.Location = new System.Drawing.Point(123, 194);
            this.tbAETelephone.Name = "tbAETelephone";
            this.tbAETelephone.Size = new System.Drawing.Size(190, 38);
            this.tbAETelephone.TabIndex = 34;
            // 
            // p2022_Appli_AdministrationDataSet4
            // 
            this.p2022_Appli_AdministrationDataSet4.DataSetName = "P2022_Appli_AdministrationDataSet4";
            this.p2022_Appli_AdministrationDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet4;
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // AjouterEtudiant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(346, 393);
            this.Controls.Add(this.tbAETelephone);
            this.Controls.Add(this.tbAEMail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbClasse);
            this.Controls.Add(this.bRetour);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Controls.Add(this.tbAEPrenom);
            this.Controls.Add(this.tbAENom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lPrenom);
            this.Controls.Add(this.lNom);
            this.Font = new System.Drawing.Font("MV Boli", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.ForeColor = System.Drawing.Color.Blue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "AjouterEtudiant";
            this.Text = "AjouterEtudiant";
            this.Load += new System.EventHandler(this.AjouterEtudiant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022AppliAdministrationDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022AppliAdministrationDataSetBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lNom;
        private System.Windows.Forms.Label lPrenom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAENom;
        private System.Windows.Forms.TextBox tbAEPrenom;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bRetour;
        private System.Windows.Forms.ComboBox cbClasse;
        private System.Windows.Forms.BindingSource p2022AppliAdministrationDataSetBindingSource1;
        private P2022_Appli_AdministrationDataSet p2022_Appli_AdministrationDataSet;
        private System.Windows.Forms.BindingSource p2022AppliAdministrationDataSetBindingSource;
        private P2022_Appli_AdministrationDataSet1 p2022_Appli_AdministrationDataSet1;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter classeTableAdapter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbAEMail;
        private System.Windows.Forms.TextBox tbAETelephone;
        private P2022_Appli_AdministrationDataSet4 p2022_Appli_AdministrationDataSet4;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private P2022_Appli_AdministrationDataSet4TableAdapters.etudiantTableAdapter etudiantTableAdapter;
    }
}