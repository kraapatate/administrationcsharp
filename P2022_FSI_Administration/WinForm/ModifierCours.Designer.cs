﻿namespace P2022_FSI_Administration.WinForm
{
    partial class ModifierCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.cbClasse = new System.Windows.Forms.ComboBox();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.tbAEDescCours = new System.Windows.Forms.TextBox();
            this.tbAENomCours = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lPrenom = new System.Windows.Forms.Label();
            this.lNom = new System.Windows.Forms.Label();
            this.p2022_Appli_AdministrationDataSet1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.classeTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(62, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 28);
            this.label3.TabIndex = 56;
            this.label3.Text = "Modifier un cours";
            // 
            // cbClasse
            // 
            this.cbClasse.DataSource = this.classeBindingSource;
            this.cbClasse.DisplayMember = "libelleclasse";
            this.cbClasse.FormattingEnabled = true;
            this.cbClasse.Location = new System.Drawing.Point(133, 219);
            this.cbClasse.Name = "cbClasse";
            this.cbClasse.Size = new System.Drawing.Size(190, 21);
            this.cbClasse.TabIndex = 55;
            this.cbClasse.ValueMember = "idclasse";
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(177, 287);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(164, 40);
            this.bEnregistrer.TabIndex = 54;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(12, 287);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(159, 40);
            this.bEffacer.TabIndex = 53;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            // 
            // tbAEDescCours
            // 
            this.tbAEDescCours.Location = new System.Drawing.Point(133, 154);
            this.tbAEDescCours.Name = "tbAEDescCours";
            this.tbAEDescCours.Size = new System.Drawing.Size(190, 20);
            this.tbAEDescCours.TabIndex = 52;
            // 
            // tbAENomCours
            // 
            this.tbAENomCours.Location = new System.Drawing.Point(133, 80);
            this.tbAENomCours.Name = "tbAENomCours";
            this.tbAENomCours.Size = new System.Drawing.Size(190, 20);
            this.tbAENomCours.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 50;
            this.label1.Text = "Classe";
            // 
            // lPrenom
            // 
            this.lPrenom.AutoSize = true;
            this.lPrenom.Location = new System.Drawing.Point(44, 157);
            this.lPrenom.Name = "lPrenom";
            this.lPrenom.Size = new System.Drawing.Size(58, 13);
            this.lPrenom.TabIndex = 49;
            this.lPrenom.Text = "description";
            // 
            // lNom
            // 
            this.lNom.AutoSize = true;
            this.lNom.Location = new System.Drawing.Point(64, 83);
            this.lNom.Name = "lNom";
            this.lNom.Size = new System.Drawing.Size(29, 13);
            this.lNom.TabIndex = 48;
            this.lNom.Text = "Nom";
            // 
            // p2022_Appli_AdministrationDataSet1
            // 
            this.p2022_Appli_AdministrationDataSet1.DataSetName = "P2022_Appli_AdministrationDataSet1";
            this.p2022_Appli_AdministrationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            this.classeBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet1;
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // ModifierCours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbClasse);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Controls.Add(this.tbAEDescCours);
            this.Controls.Add(this.tbAENomCours);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lPrenom);
            this.Controls.Add(this.lNom);
            this.Name = "ModifierCours";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ModifierCours_Load);
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbClasse;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.TextBox tbAEDescCours;
        private System.Windows.Forms.TextBox tbAENomCours;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lPrenom;
        private System.Windows.Forms.Label lNom;
        private P2022_Appli_AdministrationDataSet1 p2022_Appli_AdministrationDataSet1;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter classeTableAdapter;
    }
}