﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ModifierEtudiant : Form
    {// Utilisation des classes pour la connexion à postgresql
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;

        // attribution id de l'étudiant
        int id;
        public ModifierEtudiant(int id, string nom, string prenom, int idclasse, string mail, string tel)
        {
            InitializeComponent();
            this.id = id;

            // attribution des variables aux champs
            tbAENom.Text = nom;
            tbPrenom.Text = prenom;
            tbClasse.Text = idclasse.ToString();
            tbMail.Text = mail;
            tbTelephone.Text = tel;

        }

        private void reInitialisation()
        {
            // remettre à 0 les données du formulaire
            tbAENom.Text = "";
            tbPrenom.Text = "";
            tbMail.Text = "";
            tbTelephone.Text = "";

        }

        
        private void bEnregistrer_Click(object sender, EventArgs e)
        {
            // ajout des variables
            string nom = tbAENom.Text;
            string prenom = tbPrenom.Text;
            int idclasse = int.Parse(tbClasse.Text);
            string mail = tbMail.Text;
            string tel = tbTelephone.Text;

            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=OLEN@bts2022;";
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion.Open();

                // requete sql pour modifier les données de l'étudiant
                string comm = $"UPDATE etudiant SET nometudiant = '{nom}', prenometudiant = '{prenom}', idclasse= '{idclasse}', mailetudiant= '{mail}', teletudiant= '{tel}'" +
                    $"where idetudiant = {id};";

                var cmd = new NpgsqlCommand(comm, maConnexion);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Etudiant modifié");
                reInitialisation();

            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }
            this.Close();
        }
    }
    }

