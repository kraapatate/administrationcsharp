﻿using P2022_FSI_Administration.Classe;
using P2022_FSI_Administration.WinForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration
{
    public partial class Accueil : Form
    {
        Utilisateur uti;
        Section cla;
        Cours cou;

        public Accueil(Utilisateur uticonnecte)
        {
            InitializeComponent();
            uti = uticonnecte;
            Form formConnexion = new Connexion();
            formConnexion.Close();
           // tbUserConnecte.Text = uti.LoginUtilisateur;
        }

        public Accueil(Section cla)
        {
            this.cla = cla;
        }



        private void accueilToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
        }


        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            PanelInterieur.BringToFront();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
        }
      

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void bQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void listeDesClassesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form formListeClasse = new ListeClasse(cla);
            formListeClasse.Show();
        }

        private void ajouterUneClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterClasse = new AjouterClasse();
            formAjouterClasse.Show();
        }

        private void listeCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeCours = new ListeCours(cou);
            formListeCours.Show();
        }
        
        private void ajouterUnCoursToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form formAjouterCours = new AjouterCours();
            formAjouterCours.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            

        }
    }
}
