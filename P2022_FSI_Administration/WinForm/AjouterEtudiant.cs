﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration
{
    public partial class AjouterEtudiant : Form
    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;
        public AjouterEtudiant()
        {
            InitializeComponent();
        }

        private void bEffacer_Click(object sender, EventArgs e)
        {
            reInitialisation();
        }

        private void bRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bEnregistrer_Click(object sender, EventArgs e)
        {
            //Enregistrer en BDD
            string nom = tbAENom.Text;
            string prenom = tbAEPrenom.Text;
            string telephone = tbAETelephone.Text;
            string mail = tbAEMail.Text;
            int idclasse = cbClasse.SelectedIndex;
            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=OLEN@bts2022;";
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion.Open();
                string insert = "INSERT INTO etudiant (nometudiant, prenometudiant, idclasse, mailetudiant, teletudiant) values ( :nom, :prenom, :idclasse, :mail, :telephone )";
                commande = new NpgsqlCommand(insert, maConnexion);
                commande.Parameters.Add(new NpgsqlParameter("nom", NpgsqlDbType.Varchar)).Value = nom;
                commande.Parameters.Add(new NpgsqlParameter("prenom", NpgsqlDbType.Varchar)).Value = prenom;
                commande.Parameters.Add(new NpgsqlParameter("idclasse", NpgsqlDbType.Integer)).Value = idclasse;
                commande.Parameters.Add(new NpgsqlParameter("mail", NpgsqlDbType.Varchar)).Value = mail;
                commande.Parameters.Add(new NpgsqlParameter("telephone", NpgsqlDbType.Varchar)).Value = telephone;


                commande.Prepare();
                commande.CommandType = CommandType.Text;
                commande.ExecuteNonQuery();
                MessageBox.Show("Etudiant ajouté");
                reInitialisation();

            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }
            this.Close();

        }
        private void reInitialisation()
        {
            tbAENom.Text = "";
            tbAEPrenom.Text = "";
            cbClasse.SelectedIndex = 0;
            tbAEMail.Text = "";
            tbAETelephone.Text = "";


        }

        private void AjouterEtudiant_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet4.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.etudiantTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet4.etudiant);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet1.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet1.classe);

        }

        private void lNom_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void tbAENom_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbAEPrenom_TextChanged(object sender, EventArgs e)
        {

        }

        private void lPrenom_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cbClasse_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}