﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using P2022_FSI_Administration.Classe;
using P2022_FSI_Administration.WinForm;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ListeCours : Form

    {
        Utilisateur uti;
        Section cla;
        Cours cou;

        public ListeCours(Cours couconnecte)
        {
            InitializeComponent();
            cou = couconnecte;
          
          
        }

        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }
        

        private void bFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListeCours_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet2.cours'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.coursTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet2.cours);

        }

        private void bQuitter_Click_1(object sender, EventArgs e)
        
            {
                Application.Exit();
            }

        private void ajouterCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterCours = new AjouterCours();
            formAjouterCours.Show();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeClasse = new ListeClasse(cla);
            formListeClasse.Show();
        }

        private void ajouterUneClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterClasse = new AjouterClasse();
            formAjouterClasse.Show();
        }

        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeCours = new ListeCours(cou);
            formListeCours.Show();
        }

        

        private void dgvListeCours_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            var cells = dgvListeCours.Rows[e.RowIndex].Cells; // récupère l'objet de la cellule cliquée
                                                              // l'index de la cellule 
            var idcours = (int)cells[0].Value;
            var nom = (string)cells[1].Value;   // première valeur contenue dans la cellule
            var info = (string)cells[2].Value;
            var idclasse = (int)cells[3].Value;


            new ModifierCours(idcours,nom, info, idclasse)
                .Show();    
        }
    }

    internal class Matiere
    {
    }
}
