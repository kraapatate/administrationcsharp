﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using P2022_FSI_Administration.Classe;
using P2022_FSI_Administration.WinForm;


namespace P2022_FSI_Administration.WinForm
{
   
    public partial class ListeClasse : Form
    {
        Utilisateur uti;
        Section cla;
        Cours cou;
     //   private BitVector32.Section cla1;

        public ListeClasse(Section claconnecte)
        {
            InitializeComponent();
            cla = claconnecte;
        
        }

       // public ListeClasse(BitVector32.Section cla1)
       // {
         //   this.cla1 = cla1;
       // }

        private void ListeClasse_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet1.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet1.classe);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
                var cells = dataGridView1.Rows[e.RowIndex].Cells; // récupère l'objet de la cellule cliquée
                                                                  // l'index de la cellule 
            
                var id = (int)cells[0].Value;
                var nomClasse = (string)cells[1].Value;

            new ModifierClasse( id,nomClasse)
                    .Show();
           
        }

        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void bFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void bQuitter_Click_1(object sender, EventArgs e)
        {
            
                Application.Exit();
            }

        

        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeCours = new ListeCours(cou);
            formListeCours.Show();
        }

        private void ajouterDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterCours = new AjouterCours();
            formAjouterCours.Show();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeClasse = new ListeClasse(cla);
            formListeClasse.Show();
        }

        private void ajouterUneClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterClasse = new AjouterClasse();
            formAjouterClasse.Show();
        }

    }
}
