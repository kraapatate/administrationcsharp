﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class AjouterClasse : Form
    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;
        public AjouterClasse()
        {
            InitializeComponent();
        }
        private void bEffacer_Click(object sender, EventArgs e)
        {
            reInitialisation();
        }

        private void bRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    

        private void bEnregistrer_Click_1(object sender, EventArgs e)
        {
            string libelleclasse = tbAEPrenom.Text;

            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=OLEN@bts2022;";
                maConnexion = new NpgsqlConnection(connexion);

                maConnexion.Open();
                string insert = "INSERT INTO classe (libelleclasse) values (:libelleclasse);";
                commande = new NpgsqlCommand(insert, maConnexion);
                commande.Parameters.Add(new NpgsqlParameter("libelleclasse", NpgsqlDbType.Varchar)).Value = libelleclasse;
                commande.Prepare();
                commande.CommandType = CommandType.Text;
                commande.ExecuteNonQuery();
                MessageBox.Show("Classe ajouté");
                reInitialisation();

            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }
        }
        private void reInitialisation()
        {

            tbAEPrenom.Text = "";
        }


        private void AjouterClasse_Load_1(object sender, EventArgs e)
        {

        }

        
    }
    }
    

