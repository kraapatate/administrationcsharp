﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ModifierCours : Form
    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;

        int id;
        public ModifierCours(int id,string nom, string info, int idclasse)
        {
            InitializeComponent();
            this.id = id;

            tbAENomCours.Text = nom;
            tbAEDescCours.Text = info;
            cbClasse.Text = idclasse.ToString();
            
        }
        private void reInitialisation()
        {
            tbAENomCours.Text = "";
            tbAEDescCours.Text = "";
            cbClasse.SelectedIndex = 0;
        }

        private void bEnregistrer_Click(object sender, EventArgs e)
        {

            // ajout des variables
            string nom = tbAENomCours.Text;
            string info = tbAEDescCours.Text;
            int idclasse = cbClasse.SelectedIndex;

            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=OLEN@bts2022;";
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion.Open();

 

                // requete sql pour modifier les données de l'étudiant
                string comm = $"UPDATE cours SET libellecours = '{nom}', descriptioncours = '{info}', idclasse= '{idclasse}'" +
                    $"where idcours = {id};";

                var cmd = new NpgsqlCommand(comm, maConnexion);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Cours modifié");
                reInitialisation();
            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }

            this.Close();
        }

        private void ModifierCours_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet1.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet1.classe);

        }
    }
}
