﻿using Npgsql;
using NpgsqlTypes;
using P2022_FSI_Administration.Classe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration
{
    public partial class Connexion : Form
    {
        public Connexion()
        {
            InitializeComponent();
        }

        private bool authentification(string pwduser, string pwdfom)
        {
            //Connexion à la BDD PostGreSQL
            if (pwduser == pwdfom) return true;
            else
                return false;

        }
        private void bConnexion_Click(object sender, EventArgs e)
        {
            string loginUtiform = tbLogin.Text;
            string mdpUtiform = tbMdp.Text;
            //Contrôle de la connexion
            string Conx = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=AQWzsx123!;";
            NpgsqlConnection MyCnx = new NpgsqlConnection(Conx);
            MyCnx = new NpgsqlConnection(Conx);
            MyCnx.Open();
            string select = "SELECT * FROM utilisateur where loginutilisateur = :login;";
            NpgsqlCommand MyCmd = new NpgsqlCommand(select, MyCnx);
            MyCmd.Parameters.Add(new NpgsqlParameter("login", NpgsqlDbType.Varchar)).Value = loginUtiform;
            NpgsqlDataReader dr = MyCmd.ExecuteReader();

            if (dr.Read())
            {
                // Création de l'objet utilisateur
                int idUti = dr.GetInt32(0);
                string loginUti = dr.GetString(1);
                string mdpUti = dr.GetString(2);
                Utilisateur uti = new Utilisateur(idUti, loginUti, mdpUti);

                //Ouverture du formulaire d'accueil si la connexion est ok
                if (authentification(mdpUti, mdpUtiform))
                {
                    this.Hide();
                    Form formAccueil = new Accueil(uti);
                    formAccueil.Show();

                }
                else
                {
                    MessageBox.Show("Erreur d'authentification");
                    tbLogin.Text = "";
                    tbMdp.Text = "";
                }
            }


            MyCnx.Close();


        }
    }
}
