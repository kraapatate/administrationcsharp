﻿
namespace P2022_FSI_Administration
{
    partial class ListeEtudiant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListeEtudiant));
            this.PanelLogo = new System.Windows.Forms.Panel();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.PanelQuitter = new System.Windows.Forms.Panel();
            this.bQuitter = new System.Windows.Forms.Button();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.msGlobal = new System.Windows.Forms.MenuStrip();
            this.accueilToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionEtudiantToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesEtudiantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnEtudiantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesClassesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUneClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelInterieur = new System.Windows.Forms.Panel();
            this.dgvListeEtudiant = new System.Windows.Forms.DataGridView();
            this.etudiantBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.etudiantTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSetTableAdapters.etudiantTableAdapter();
            this.bFermer = new System.Windows.Forms.Button();
            this.etudiantBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.etudiantBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet3 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet3();
            this.etudiantBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.etudiantTableAdapter1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet3TableAdapters.etudiantTableAdapter();
            this.idetudiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nometudiantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenometudiantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idclasseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailetudiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teletudiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.PanelQuitter.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            this.msGlobal.SuspendLayout();
            this.PanelInterieur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeEtudiant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource4)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLogo
            // 
            this.PanelLogo.Controls.Add(this.pbLogo);
            this.PanelLogo.Location = new System.Drawing.Point(0, 0);
            this.PanelLogo.Name = "PanelLogo";
            this.PanelLogo.Size = new System.Drawing.Size(84, 90);
            this.PanelLogo.TabIndex = 10;
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.Location = new System.Drawing.Point(3, 0);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(78, 87);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 0;
            this.pbLogo.TabStop = false;
            // 
            // PanelQuitter
            // 
            this.PanelQuitter.Controls.Add(this.bQuitter);
            this.PanelQuitter.Location = new System.Drawing.Point(899, 0);
            this.PanelQuitter.Name = "PanelQuitter";
            this.PanelQuitter.Size = new System.Drawing.Size(123, 46);
            this.PanelQuitter.TabIndex = 11;
            // 
            // bQuitter
            // 
            this.bQuitter.BackColor = System.Drawing.Color.LightCyan;
            this.bQuitter.Location = new System.Drawing.Point(4, 3);
            this.bQuitter.Name = "bQuitter";
            this.bQuitter.Size = new System.Drawing.Size(115, 40);
            this.bQuitter.TabIndex = 7;
            this.bQuitter.Text = "QUITTER";
            this.bQuitter.UseVisualStyleBackColor = false;
            this.bQuitter.Click += new System.EventHandler(this.bQuitter_Click);
            // 
            // PanelMenu
            // 
            this.PanelMenu.Controls.Add(this.msGlobal);
            this.PanelMenu.Location = new System.Drawing.Point(87, 0);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(806, 29);
            this.PanelMenu.TabIndex = 12;
            // 
            // msGlobal
            // 
            this.msGlobal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accueilToolStripMenuItem2,
            this.gestionEtudiantToolStripMenuItem1,
            this.gestionClasseToolStripMenuItem,
            this.gestionCoursToolStripMenuItem});
            this.msGlobal.Location = new System.Drawing.Point(0, 0);
            this.msGlobal.Name = "msGlobal";
            this.msGlobal.Size = new System.Drawing.Size(806, 24);
            this.msGlobal.TabIndex = 0;
            this.msGlobal.Text = "Menu";
            // 
            // accueilToolStripMenuItem2
            // 
            this.accueilToolStripMenuItem2.Name = "accueilToolStripMenuItem2";
            this.accueilToolStripMenuItem2.Size = new System.Drawing.Size(58, 20);
            this.accueilToolStripMenuItem2.Text = "Accueil";
            this.accueilToolStripMenuItem2.Click += new System.EventHandler(this.accueilToolStripMenuItem2_Click);
            // 
            // gestionEtudiantToolStripMenuItem1
            // 
            this.gestionEtudiantToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesEtudiantsToolStripMenuItem,
            this.ajouterUnEtudiantToolStripMenuItem});
            this.gestionEtudiantToolStripMenuItem1.Name = "gestionEtudiantToolStripMenuItem1";
            this.gestionEtudiantToolStripMenuItem1.Size = new System.Drawing.Size(106, 20);
            this.gestionEtudiantToolStripMenuItem1.Text = "Gestion Etudiant";
            // 
            // listeDesEtudiantsToolStripMenuItem
            // 
            this.listeDesEtudiantsToolStripMenuItem.Name = "listeDesEtudiantsToolStripMenuItem";
            this.listeDesEtudiantsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.listeDesEtudiantsToolStripMenuItem.Text = "Liste des étudiants";
            this.listeDesEtudiantsToolStripMenuItem.Click += new System.EventHandler(this.listeDesEtudiantsToolStripMenuItem_Click);
            // 
            // ajouterUnEtudiantToolStripMenuItem
            // 
            this.ajouterUnEtudiantToolStripMenuItem.Name = "ajouterUnEtudiantToolStripMenuItem";
            this.ajouterUnEtudiantToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.ajouterUnEtudiantToolStripMenuItem.Text = "Ajouter un étudiant";
            this.ajouterUnEtudiantToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnEtudiantToolStripMenuItem_Click);
            // 
            // gestionClasseToolStripMenuItem
            // 
            this.gestionClasseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesClassesToolStripMenuItem,
            this.ajouterUneClasseToolStripMenuItem});
            this.gestionClasseToolStripMenuItem.Name = "gestionClasseToolStripMenuItem";
            this.gestionClasseToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.gestionClasseToolStripMenuItem.Text = "Gestion classe";
            // 
            // listeDesClassesToolStripMenuItem
            // 
            this.listeDesClassesToolStripMenuItem.Name = "listeDesClassesToolStripMenuItem";
            this.listeDesClassesToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.listeDesClassesToolStripMenuItem.Text = "Liste des classes";
            this.listeDesClassesToolStripMenuItem.Click += new System.EventHandler(this.listeDesClassesToolStripMenuItem_Click);
            // 
            // ajouterUneClasseToolStripMenuItem
            // 
            this.ajouterUneClasseToolStripMenuItem.Name = "ajouterUneClasseToolStripMenuItem";
            this.ajouterUneClasseToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.ajouterUneClasseToolStripMenuItem.Text = "Ajouter une classe";
            this.ajouterUneClasseToolStripMenuItem.Click += new System.EventHandler(this.ajouterUneClasseToolStripMenuItem_Click_1);
            // 
            // gestionCoursToolStripMenuItem
            // 
            this.gestionCoursToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesCoursToolStripMenuItem,
            this.ajouterCoursToolStripMenuItem});
            this.gestionCoursToolStripMenuItem.Name = "gestionCoursToolStripMenuItem";
            this.gestionCoursToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.gestionCoursToolStripMenuItem.Text = "Gestion cours";
            // 
            // listeDesCoursToolStripMenuItem
            // 
            this.listeDesCoursToolStripMenuItem.Name = "listeDesCoursToolStripMenuItem";
            this.listeDesCoursToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.listeDesCoursToolStripMenuItem.Text = "Liste des cours";
            this.listeDesCoursToolStripMenuItem.Click += new System.EventHandler(this.listeDesCoursToolStripMenuItem_Click);
            // 
            // ajouterCoursToolStripMenuItem
            // 
            this.ajouterCoursToolStripMenuItem.Name = "ajouterCoursToolStripMenuItem";
            this.ajouterCoursToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.ajouterCoursToolStripMenuItem.Text = "Ajouter cours";
            this.ajouterCoursToolStripMenuItem.Click += new System.EventHandler(this.ajouterCoursToolStripMenuItem_Click);
            // 
            // PanelInterieur
            // 
            this.PanelInterieur.Controls.Add(this.dgvListeEtudiant);
            this.PanelInterieur.Location = new System.Drawing.Point(87, 35);
            this.PanelInterieur.Name = "PanelInterieur";
            this.PanelInterieur.Size = new System.Drawing.Size(806, 482);
            this.PanelInterieur.TabIndex = 12;
            // 
            // dgvListeEtudiant
            // 
            this.dgvListeEtudiant.AllowUserToOrderColumns = true;
            this.dgvListeEtudiant.AutoGenerateColumns = false;
            this.dgvListeEtudiant.BackgroundColor = System.Drawing.Color.White;
            this.dgvListeEtudiant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListeEtudiant.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idetudiant,
            this.nometudiantDataGridViewTextBoxColumn,
            this.prenometudiantDataGridViewTextBoxColumn,
            this.idclasseDataGridViewTextBoxColumn,
            this.mailetudiant,
            this.teletudiant});
            this.dgvListeEtudiant.DataSource = this.etudiantBindingSource4;
            this.dgvListeEtudiant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListeEtudiant.GridColor = System.Drawing.Color.SkyBlue;
            this.dgvListeEtudiant.Location = new System.Drawing.Point(0, 0);
            this.dgvListeEtudiant.Name = "dgvListeEtudiant";
            this.dgvListeEtudiant.Size = new System.Drawing.Size(806, 482);
            this.dgvListeEtudiant.TabIndex = 0;
            this.dgvListeEtudiant.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListeEtudiant_CellContentClick);
            // 
            // etudiantBindingSource1
            // 
            this.etudiantBindingSource1.DataMember = "etudiant";
            this.etudiantBindingSource1.DataSource = this.p2022_Appli_AdministrationDataSet;
            // 
            // p2022_Appli_AdministrationDataSet
            // 
            this.p2022_Appli_AdministrationDataSet.DataSetName = "P2022_Appli_AdministrationDataSet";
            this.p2022_Appli_AdministrationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet;
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // bFermer
            // 
            this.bFermer.BackColor = System.Drawing.Color.LightCyan;
            this.bFermer.Location = new System.Drawing.Point(899, 52);
            this.bFermer.Name = "bFermer";
            this.bFermer.Size = new System.Drawing.Size(115, 40);
            this.bFermer.TabIndex = 13;
            this.bFermer.Text = "FERMER";
            this.bFermer.UseVisualStyleBackColor = false;
            this.bFermer.Click += new System.EventHandler(this.bFermer_Click);
            // 
            // etudiantBindingSource2
            // 
            this.etudiantBindingSource2.DataMember = "etudiant";
            this.etudiantBindingSource2.DataSource = this.p2022_Appli_AdministrationDataSet;
            // 
            // etudiantBindingSource3
            // 
            this.etudiantBindingSource3.DataMember = "etudiant";
            this.etudiantBindingSource3.DataSource = this.p2022_Appli_AdministrationDataSet;
            // 
            // p2022_Appli_AdministrationDataSet3
            // 
            this.p2022_Appli_AdministrationDataSet3.DataSetName = "P2022_Appli_AdministrationDataSet3";
            this.p2022_Appli_AdministrationDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource4
            // 
            this.etudiantBindingSource4.DataMember = "etudiant";
            this.etudiantBindingSource4.DataSource = this.p2022_Appli_AdministrationDataSet3;
            // 
            // etudiantTableAdapter1
            // 
            this.etudiantTableAdapter1.ClearBeforeFill = true;
            // 
            // idetudiant
            // 
            this.idetudiant.DataPropertyName = "idetudiant";
            this.idetudiant.HeaderText = "idetudiant";
            this.idetudiant.Name = "idetudiant";
            this.idetudiant.ReadOnly = true;
            this.idetudiant.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // nometudiantDataGridViewTextBoxColumn
            // 
            this.nometudiantDataGridViewTextBoxColumn.DataPropertyName = "nometudiant";
            this.nometudiantDataGridViewTextBoxColumn.HeaderText = "nom de l\'étudiant";
            this.nometudiantDataGridViewTextBoxColumn.MinimumWidth = 230;
            this.nometudiantDataGridViewTextBoxColumn.Name = "nometudiantDataGridViewTextBoxColumn";
            this.nometudiantDataGridViewTextBoxColumn.ReadOnly = true;
            this.nometudiantDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.nometudiantDataGridViewTextBoxColumn.Width = 230;
            // 
            // prenometudiantDataGridViewTextBoxColumn
            // 
            this.prenometudiantDataGridViewTextBoxColumn.DataPropertyName = "prenometudiant";
            this.prenometudiantDataGridViewTextBoxColumn.HeaderText = "prénom de l\'étudiant";
            this.prenometudiantDataGridViewTextBoxColumn.MinimumWidth = 230;
            this.prenometudiantDataGridViewTextBoxColumn.Name = "prenometudiantDataGridViewTextBoxColumn";
            this.prenometudiantDataGridViewTextBoxColumn.ReadOnly = true;
            this.prenometudiantDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.prenometudiantDataGridViewTextBoxColumn.Width = 230;
            // 
            // idclasseDataGridViewTextBoxColumn
            // 
            this.idclasseDataGridViewTextBoxColumn.DataPropertyName = "idclasse";
            this.idclasseDataGridViewTextBoxColumn.HeaderText = "Libellé de la classe";
            this.idclasseDataGridViewTextBoxColumn.MinimumWidth = 230;
            this.idclasseDataGridViewTextBoxColumn.Name = "idclasseDataGridViewTextBoxColumn";
            this.idclasseDataGridViewTextBoxColumn.ReadOnly = true;
            this.idclasseDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.idclasseDataGridViewTextBoxColumn.Width = 230;
            // 
            // mailetudiant
            // 
            this.mailetudiant.DataPropertyName = "mailetudiant";
            this.mailetudiant.HeaderText = "mailetudiant";
            this.mailetudiant.Name = "mailetudiant";
            this.mailetudiant.ReadOnly = true;
            this.mailetudiant.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // teletudiant
            // 
            this.teletudiant.DataPropertyName = "teletudiant";
            this.teletudiant.HeaderText = "teletudiant";
            this.teletudiant.Name = "teletudiant";
            this.teletudiant.ReadOnly = true;
            this.teletudiant.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ListeEtudiant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 529);
            this.Controls.Add(this.bFermer);
            this.Controls.Add(this.PanelInterieur);
            this.Controls.Add(this.PanelMenu);
            this.Controls.Add(this.PanelQuitter);
            this.Controls.Add(this.PanelLogo);
            this.Font = new System.Drawing.Font("MV Boli", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.ForeColor = System.Drawing.Color.Blue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ListeEtudiant";
            this.Text = "Liste des Etudiants";
            this.Load += new System.EventHandler(this.ListeEtudiant_Load);
            this.PanelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.PanelQuitter.ResumeLayout(false);
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.msGlobal.ResumeLayout(false);
            this.msGlobal.PerformLayout();
            this.PanelInterieur.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeEtudiant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelLogo;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Panel PanelQuitter;
        private System.Windows.Forms.Button bQuitter;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.MenuStrip msGlobal;
        private System.Windows.Forms.ToolStripMenuItem accueilToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gestionEtudiantToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listeDesEtudiantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnEtudiantToolStripMenuItem;
        private System.Windows.Forms.Panel PanelInterieur;
        private System.Windows.Forms.DataGridView dgvListeEtudiant;
        private P2022_Appli_AdministrationDataSet p2022_Appli_AdministrationDataSet;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private P2022_Appli_AdministrationDataSetTableAdapters.etudiantTableAdapter etudiantTableAdapter;
        private System.Windows.Forms.Button bFermer;
        private System.Windows.Forms.ToolStripMenuItem gestionClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesClassesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUneClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionCoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesCoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterCoursToolStripMenuItem;
        private System.Windows.Forms.BindingSource etudiantBindingSource1;
        private System.Windows.Forms.BindingSource etudiantBindingSource2;
        private System.Windows.Forms.BindingSource etudiantBindingSource3;
        private P2022_Appli_AdministrationDataSet3 p2022_Appli_AdministrationDataSet3;
        private System.Windows.Forms.BindingSource etudiantBindingSource4;
        private P2022_Appli_AdministrationDataSet3TableAdapters.etudiantTableAdapter etudiantTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idetudiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn nometudiantDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenometudiantDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idclasseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mailetudiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn teletudiant;
    }
}