﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2022_FSI_Administration.Classe
{
    class Professeur
    {
        private int id;
        private string Prenom;
        private string Nom;

        public Professeur(int id, string prenom, string nom)
        {
            this.id = id;
            Prenom = prenom;
            Nom = nom;
        }

        public int Id { get => id; set => id = value; }
        public string Prenom1 { get => Prenom; set => Prenom = value; }
        public string Nom1 { get => Nom; set => Nom = value; }
    }

}
